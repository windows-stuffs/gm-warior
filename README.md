# GM warior

GM warior is a small systray utility, which dissable/enable settings of the
SecondLife viewer by press key combination.

**Settings, which will be changed:**
* Always Run (Ctrl+R)
* Rendering Types -> Alpha (Ctrl+Alt+Shift+2)
* Rendering Types -> Tree (Ctrl+Alt+Shift+3)
* Rendering Types -> Water (Ctrl+Alt+Shift+7)
* Rendering Types -> Ground (Ctrl+Alt+Shift+8)
* Rendering Types -> Grass (Ctrl+Alt+Shift+0)
* Rendering Types -> Clouds (Ctrl+Alt+Shift+-)
* Rendering Types -> Tree (Ctrl+Alt+Shift+3)
* Rendering Types -> Particles (Ctrl+Alt+Shift+=)
* Rendering Features -> Foot shadows (Ctrl+Alt+F5)
* Rendering Features -> Fog (Ctrl+Alt+F6)
* Rendering Features -> Test RFInfo (Ctrl+Alt+F8)

## Howto use GM warrior
1. double click at `GM warior.exe`. This utility will be enabled like tray icon
2. Press key combination `Win+F12` or `Alt+F12`. This send all the shortcuts for set your viewer
3. Press `Win+D` for pause this utility or `Win+Q` for exit

## Build from the source
For build this software you will need:
* AutoHotkey 1.0.4805 (https://autohotkey.com/)
* Compile AHK (https://github.com/mercury233/compile-ahk/)
* SmartGUI Creator (https://autohotkey.com/board/topic/738-smartgui-creator/)
