;
; GM warior 1.0
;

;Variables
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#Persistent
#SingleInstance

status := 0

;---------------------------------
;---------------------------------

Menu, Tray, NoStandard

Menu, tray, add, Help   Win+F1, myHelp
Menu, tray, add
Menu, tray, add, Pause   Win+D, myPause
Menu, tray, add
Menu, tray, add, Exit       Win+Q, myExit
return

;---------------------------------

myExit:
    ExitApp
    return

;---------------------------------

myPause:
    Suspend, Toggle
    Menu, Tray, ToggleCheck, Pause   Win+D
    return

;---------------------------------

myHelp:
    TrayTip, Help, For change settings press key combination 'WIN+F12' or 'ALT+F12'.
    SetTimer, RemoveTrayTip, 5000

;---------------------------------

RemoveTrayTip:
    SetTimer, RemoveTrayTip, Off
    TrayTip
    return

;---------------------------------

mySendShortcuts()
{
    Send, {SHIFTDOWN}{CTRLDOWN}{ALTDOWN}2{ALTUP}{S HIFTUP}{CTRLUP}
    Send, {SHIFTDOWN}{CTRLDOWN}{ALTDOWN}3{ALTUP}{S HIFTUP}{CTRLUP}
    Send, {SHIFTDOWN}{CTRLDOWN}{ALTDOWN}7{ALTUP}{S HIFTUP}{CTRLUP}
    Send, {SHIFTDOWN}{CTRLDOWN}{ALTDOWN}8{ALTUP}{S HIFTUP}{CTRLUP}
    Send, {SHIFTDOWN}{CTRLDOWN}{ALTDOWN}0{ALTUP}{S HIFTUP}{CTRLUP}
    Send, {SHIFTDOWN}{CTRLDOWN}{ALTDOWN}-{ALTUP}{S HIFTUP}{CTRLUP}
    Send, {SHIFTDOWN}{CTRLDOWN}{ALTDOWN}={ALTUP}{S HIFTUP}{CTRLUP}
    Send, {SHIFTDOWN}{CTRLDOWN}Y{ALTUP}{CTRLUP}
    Send, {CTRLDOWN}{ALTDOWN}{F5}}{CTRLUP}{ALTUP}
    Send, {CTRLDOWN}{ALTDOWN}{F6}}{CTRLUP}{ALTUP}
    Send, {CTRLDOWN}{ALTDOWN}{F8}}{CTRLUP}{ALTUP}
    Send, {CTRLDOWN}{ALTDOWN}{SHIFTDOWN}\{SHIFTUP} {CTRLUP}{ALTUP}
    Send, {CTRLDOWN}r{CTRLUP}
    TrayTip, Change settings, Shortcuts for change the settings was sent.
    SetTimer, RemoveTrayTip, 1000
    return
}


;---------------------------------

; Define shortcuts

;---------------------------------
;---------------------------------

#q::ExitApp

#s::
   Suspend, Toggle
   Menu, Tray, ToggleCheck, Pause   Win+D
   return

;---------------------------------

; Just for Second Life
;IfWinActive ahk_class Second Life

;---------------------------------

; Win + F12
#F12::
    mySendShortcuts()
    return

; ALT + F12
!F12::
    mySendShortcuts()
    return

;---------------------------------

;IfWinActive
; End Just for Second Life

;---------------------------------

return
